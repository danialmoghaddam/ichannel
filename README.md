# README #


- **~~Task 1~~**
- **~~Task 2~~**
- **~~Task 3~~**

Total hours spent: ~ **20**

### Architecture ###

The development was mainly focused on MVC design pattern while taking advantage of React Native (Sort/Favorite List/Cloud sync reaction to events) and Protocol-Oriented models/views. SRP (Single Responsibility Principle) was widly practiced in almost all managers/helpers/manipulators.

### Features ###

* Channel listing
* Channel listing pagination
* Dynamic memory caching
* Sort: 1. Channel title (Alphabetically A-Z) 2. Channel number (Numerically 1-9) 3. Favorite on top
* Favorite marking
* Favorite listing
* Currently on air listing with pagination
* Social sign-in (G+)
* Cloud 2-ways sync (sort settings + favorite channels)

### Modules ###

* Channel List: The controller is observing the sort type to re-sort the list accordingly
* Channel Row: Each channel row is observing favorite changes and update its UI accordingly
* Favorite List: The controller is observing favorite changes and update its UI accordingly
* On Air List: It's pagination-enabled. Fetches the programm from 00:00 to 23:59 of the user device localized date. Auto reload right after midnight for new day contents
* On Air Row: Elemenates the past programs. Display currently airing. Auto reload after the current program is finished.
* Cloud Sync: It uses last modified time stamp to determine the sync direction.
* Database: Firebase non-sql database.
* Authentication: Google+ authentication via Firebase

### How to run ###

The project should run out of the box without any issue. Just make sure to open `iChannel.xcworkspace`.
However, in case of dependecy issues, `cd` to project root in terminal and run `pod install`

**If you don't have CocoaPods, you can install it:**

`$ sudo gem install cocoapods`

Don't have `gem`?

Download and install it from [here](https://rubygems.org/pages/download)  

### Languages\Tools ###

* Swift 3
* CocoaPods

### 3rd Party Libraries ###

* PromiseKit: Chainable promise functions [PromiseKit](https://github.com/mxcl/PromiseKit.git)
* SwiftyJSON: this lib makes it easy to deal with JSON data in Swift [SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON.git)
* Kingfisher:  A lightweight and pure Swift implemented library for downloading and cacheing
   image from the web. [Kingfisher](https://github.com/onevcat/Kingfisher.git)
* XCGLogger: A debug log module for use in Swift projects. [XCGLogger](https://github.com/DaveWoodCom/XCGLogger.git)
* EZSwiftExtensions: A collection of swift data type and class extentions. [EZSwiftExtensions](https://github.com/goktugyil/EZSwiftExtensions.git)
* Alamofire: Elegant HTTP Networking in Swift. [Alamofire](https://github.com/Alamofire/Alamofire.git)
* SVProgressHUD: A clean and lightweight progress HUD for your iOS and tvOS app. [SVProgressHUD](https://github.com/SVProgressHUD/SVProgressHUD.git)
* RxSwift: RxSwift is a Swift implementation of Reactive Extensions. [RxSwift](https://github.com/ReactiveX/RxSwift.git)
* DZNEmptyDataSet: A drop-in UITableView/UICollectionView superclass category for showing empty
   datasets whenever the view has no content to display. [DZNEmptyDataSet](https://github.com/dzenbot/DZNEmptyDataSet.git)
* Presentr: A simple Swift wrapper for custom view controller presentations. [Presentr](https://github.com/icalialabs/Presentr.git)
* GoogleSignIn: Enables iOS apps to sign in with Google. [GoogleSignIn](https://developers.google.com/identity/sign-in/ios/)
* Firebase: Firebase Auth and Database for iOS [Firebase](https://firebase.google.com)

### To-Improve ###

* Real time cloud-to-local sync
* Navigation-enabled TV-Guide (To enable user to navigation backward/forward)
* Better graphics and colors
* App icon & splash screen