//
//  AuthenticationService.swift
//  iChannel
//
//  Created by Danial on 18/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation
import Firebase
import GoogleSignIn
import PromiseKit

class AuthenticationService: NSObject {
    let (promise, fulfill, reject) = Promise<Any>.pending()
    var retainCycle: NSObject?
    
    // MARK: - Public Methods
    typealias AuthenticationCompletion = (_ success: Bool, _ error: Error?) -> ()
    
    // MARK: - Social
    
    /**
     Sign-in to Google account in order to use it login or sign-up to our Firebase
     - parameter uiDelegate: The view controller which would be responsible to handle the navigation to google login
     page
     - parameter completion: a block which is invoked when the sign-in is completed or failed. Additionally, it would 
     return a boolean to inform the caller if user has not completed setting up the profile
     */
    func googleSignIn(uiDelegate: GIDSignInUIDelegate,
                      completion: @escaping AuthenticationCompletion) {
        
        GIDSignIn.sharedInstance().clientID = FIRApp.defaultApp()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = uiDelegate
        
        GIDSignIn.sharedInstance().promise().then { (user) -> Promise<FIRUser> in
            
            let loggedUser = user as! GIDGoogleUser
            Logger.shared.log.debug("Logged as: \(loggedUser.profile.email)")
            let credential = FIRGoogleAuthProvider.credential(withIDToken: loggedUser.authentication.idToken,
                                                              accessToken: loggedUser.authentication.accessToken)
            return self.authenticateSocialAccont(credential: credential)
            
        }.then { (user) -> () in
            
            completion(true, nil)
            
        }.catch { (error) in
            Logger.shared.log.error(error)
            completion(false, error)
        }
    }
    
    func isLoggedIn() -> Bool {
        return FIRAuth.auth()?.currentUser != nil
    }
    
    /**
     Sync logout function. Successful logout will trigger the auth user state observer
     */
    func logout() {
        try! FIRAuth.auth()!.signOut()
    }
}

// MARK: - Signup Flow
private extension AuthenticationService {
    
    func createUser(username: String,
                    email: String,
                    password: String) -> Promise<FIRUser> {
        
        return Promise { fulfill, reject in
            
            FIRAuth.auth()?.createUser(withEmail: email, password: password) { (user, error) in
                
                if let _user = user {
                    fulfill(_user)
                    
                } else if let _error = error {
                    reject(_error)
                }
                
            }
            
        }
    }
    
    func authenticateSocialAccont(credential: FIRAuthCredential) -> Promise<FIRUser> {
        return Promise { fulfill, reject in
            
            FIRAuth.auth()?.signIn(with: credential) { (user, error) in
                
                if let _user = user {
                    fulfill(_user)
                    
                } else if let _error = error {
                    reject(_error)
                }
            
            }
            
        }
    }
    
    func insertUserIntoDatabase(uid: String,
                                username: String,
                                email: String,
                                fullName: String?) -> Promise<FIRDatabaseReference> {
        
        return Promise { fulfill, reject in
            
            let ref = FireBaseSingleRequest.FireBaseReference.Users.child(uid)
            
            let account: [String: Any] = ["email": email,
                                               "creationDate": NSDate().timeIntervalSince1970]

            // Attach the params
            let param = ["account": account]
            
            ref.setValue(param, withCompletionBlock: { (error, newRef) in
                
                if let _error = error {
                    reject(_error)
                    
                } else {
                    fulfill(newRef)
                }
            })
            
        }
    }
}

// MARK: - Google SingIn Protocol
extension AuthenticationService: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!,
              didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        
        if let _currentUser = signIn.currentUser {
            fulfill(_currentUser)
        } else if let _error = error {
            reject(_error)
        } else {
            reject (NSError.generateErrorFor(description: "Failed to login using google account"))
        }
        retainCycle = nil
    }
}
