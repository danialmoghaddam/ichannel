//
//  Request.swift
//  iChannel
//
//  Created by Danial on 14/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation
import Alamofire

struct WebRequest {
    

    private static let apiBaseUrlString = APIPath.hostUrlString + APIPath.apiVersion
    
    
    private(set) var method: Alamofire.HTTPMethod
    private(set) var encoding: Alamofire.URLEncoding
    private(set) var path: String!
    private(set) var parameters: [String: Any]?
    private(set) var shouldCache: Bool = false
    private(set) var webRequest: DataRequest!
    
    /**
     Generic request class to make a web service call to a particular end point
     - parameter method: the request method such as .get, .post, etc - default is .get
     - parameter encoding: parameter encoding for this request such as QueryString, httpBody. default is .methodDependent
     - parameter path: end point path for the api. it will be appended to host and api base url
     - parameter parameters: optional web request parameters
     - parameter completion: a block which is invoked when the request is completed, or is
     failed. Invoked asynchronously on the main thread in the future.
     */
    
    init(method: Alamofire.HTTPMethod = .get,
         encoding: Alamofire.URLEncoding = .methodDependent,
         path: String, parameters: [String: Any]?,
         shouldCache: Bool = false) {
        
        self.method = method
        self.encoding = encoding
        self.path = path
        self.parameters = parameters
        self.shouldCache = shouldCache
        
        let hostURL = NSURL(string: WebRequest.apiBaseUrlString)!
        let apiURL = hostURL.appendingPathComponent(self.path)!
        
        self.webRequest = Alamofire.request(apiURL,
                                            method: method,
                                            parameters: parameters,
                                            encoding: encoding)
        
    }
    
    /**
     Cancel this request if it's ongoing
     */
    func cancel() {
        self.webRequest.cancel()
    }
}
