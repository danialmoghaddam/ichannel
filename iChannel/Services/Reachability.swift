//
//  Reachability.swift
//  iChannel
//
//  Created by Danial on 15/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation
import Alamofire

class Reachability {
    static let shared = Reachability()
    static let notificationName = "ReachabilityNotification"
    
    private let manager = NetworkReachabilityManager(host: "www.google.com")!
    private(set) var isReachable = false
    
    private init() {
        
        manager.listener = { status in
            Logger.shared.log.info("📡 Network Status Changed: \(status)")
            if status == .notReachable {
                self.isReachable = false
            } else {
                //Ignore the unknown status
                self.isReachable = true
            }
            self.broadcastStatus()
        }
        
        manager.startListening()
        self.isReachable = manager.isReachable
        
    }
    
    func start() {
        Logger.shared.log.info("Network reachability monitoring started")
    }
    
    private func broadcastStatus() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Reachability.notificationName), object: nil, userInfo: ["isReachable": self.isReachable])
    }
}
