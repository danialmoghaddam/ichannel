//
//  WebRequester.swift
//  iChannel
//
//  Created by Danial on 14/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


class WebRequester {
    /**
     Abstract class to execute a web request using a request object.
     - parameter request: the ready to invoke request object which includes path, parameters, etc
     - parameter completion: a block which is invoked when the request is completed, or is
     failed. Invoked asynchronously on the main thread in the future.
     */
    class func execute(request: WebRequest,
                       completion: @escaping (_ json: JSON?, _ error: Error?) -> ()) {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        if let cache = URLCache.shared.cachedResponse(for: request.webRequest.request!) {
            
            Logger.shared.log.debug("Cache available for API endpint: \(request.path)")
            self.evaluate(cache: cache, forRequest: request.webRequest, completion: completion)
            
        } else {
            
            request.webRequest.validate().responseJSON { (response) in
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                
                switch response.result {
                case .success(let data):
                    if (response.response != nil && response.data == nil) || response.data?.count == 0 {
                        
                        completion(nil, WebRequester.log(badResponse: response, method: request.method))
                        
                    } else {
                        
                        if request.shouldCache { self.store(response: response, forHTTPMethod: request.method) }
                        
                        let json = JSON.init(data)
                        completion(json, nil)
                    }
                    break
                    
                case .failure(let error):
                    completion(nil, error)
                    
                    break
                }
            }
            
        }
        

        
    }
    /**
     Store a web request response in url cache
     - parameter response: request response
     - parameter `forHTTPMethod`: request method
     */
    private class func store(response: DataResponse<Any>,
                             `forHTTPMethod` method: Alamofire.HTTPMethod) {
        
        if method == .get {
            
            if let rResponse = response.response,
                let rData = response.data,
                let rRequest = response.request {
                
                // save the request
                let cachedURLResponse = CachedURLResponse(
                    response: rResponse,
                    data: rData,
                    userInfo: nil,
                    storagePolicy: .allowedInMemoryOnly
                )
                Logger.shared.log.debug("Caching API response for: \(rRequest.url?.absoluteString ?? "Unknown")")
                URLCache.shared.storeCachedResponse(cachedURLResponse, for: rRequest)
            }
        }
    }
    /**
     Evaluate the cached response of a web request and convenietly return validated content or error
     - parameter cache: cached response to be validated
     - parameter `forRequest`: the request which the the cache should be used for
     */

    private class func evaluate(cache: CachedURLResponse,
                                `forRequest` request: Request,
                                completion: (_ json: JSON, _ error: Error?) -> Void) {
        
        request.cancel()
        
        var json : [String : AnyObject]? = nil
        
        do {
            
            json = try JSONSerialization.jsonObject(
                with: cache.data,
                options: .allowFragments
                ) as? [String : AnyObject]
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            let jsonObj = JSON(json!)
            completion(jsonObj, nil)
            
        } catch (let error) {
            
            Logger.shared.log.debug(error)
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            completion(JSON.null, error)
        }
        
    }


    private class func log(badResponse: DataResponse<Any>,
                           method: Alamofire.HTTPMethod) -> Error {
        
        let userInfo: [AnyHashable: Any] = [
            "title": "Server error - empty data",
            "message": "There is error on the server - empty data",
            "type": "ServerError",
            NSLocalizedDescriptionKey: "There is error on the server.\nPlease, try again later.",
            "desc": badResponse.debugDescription,
            "method": method,
            "url": badResponse.request?.url ?? "NA",
            "params": badResponse.request?.allHTTPHeaderFields ?? "",
            "errorCode": badResponse.response?.statusCode ?? 0
        ]
        
        let error = NSError.init(domain: "Internal", code: badResponse.response?.statusCode ?? 0, userInfo: userInfo)
        // TODO: this can be send to loggin server such as crashlytics as non-fatal error to be investigated
        
        return error
    }
}
