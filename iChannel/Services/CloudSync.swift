//
//  CloudSync.swift
//  iChannel
//
//  Created by Danial on 18/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase

class CloudSync {
    
    static let shared = CloudSync()
    
    enum SyncDirection {
        case none
        case toCloud
        case fromCloud
    }
    
    enum DataType: String {
        case favorite = "favoriteInfo"
        case sort = "sortInfo"
    }
    
    fileprivate var isFetching = false
    
    private init() {
        _ = ChannelSorter.shared.sortInfo.observable.subscribe(onNext:{ _ in
            self.syncIfNeeded()
        })
        
        _ = FavoriteManager.shared.favoriteInfo.observable.subscribe(onNext:{ _ in
            self.syncIfNeeded()
        })
        self.observeAppStatus()
    }
    
    func syncIfNeeded() {
        self.fetchFromCloud()
    }
}

fileprivate extension CloudSync {
    func observeAppStatus() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(fetchFromCloud),
                                               name: .UIApplicationDidBecomeActive,
                                               object: nil)
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Reachability.notificationName),
                                               object: nil,
                                               queue: nil) { (notification) in
                                                
                                                if Reachability.shared.isReachable {
                                                    self.fetchFromCloud()
                                                }
                                                
        }
    }
}

// MARK: - Determination functions
fileprivate extension CloudSync {
    /**
     Fetch latest settings form cloud
     */
    @objc func fetchFromCloud() {
        
        if self.isFetching { return }
        
        if AuthenticationService().isLoggedIn() {
            
            self.isFetching = true
            Logger.shared.log.debug("Fetching user settings from cloud ...")
            let reference = FireBaseSingleRequest.FireBaseReference.Users.child(FIRAuth.auth()!.currentUser!.uid)
            FireBaseSingleRequest.handleReadRequestWithReference(reference: reference, completion: { [weak self] (snapshot, error)  in
                
                self?.isFetching = false
                
                if let _content = snapshot?.value as? [String: Any] {
                    self?.handle(data: _content)
                } else {
                    self?.sync(cloudFavortie: [:], direction: .toCloud)
                    self?.sync(cloudSort: [:], direction: .toCloud)
                }
                
            })
        }
    }
    /**
     handle setting content received from cloud
     - parameter data: cloud content
     */
    func handle(data: [String: Any]) {
        var toSync: [DataType] = [.favorite, .sort]
        if let _favoriteInfo = data["favoriteInfo"] as? [String: Any] {
            
            let direction = self.syncType(cloudLastUpdated: _favoriteInfo["lastUpdate"] as? Int64 ?? 0,
                                         localLastUpdated: FavoriteManager.shared.favoriteInfo.lastUpdate)
            toSync = toSync.filter({$0 != .favorite})
            self.sync(cloudFavortie: _favoriteInfo, direction: direction)
        }
        
        if let _sortInfo = data["sortInfo"] as? [String: Any] {
            
            let direction = self.syncType(cloudLastUpdated: _sortInfo["lastUpdate"] as? Int64 ?? 0,
                                          localLastUpdated: ChannelSorter.shared.sortInfo.lastUpdate)
            
            toSync = toSync.filter({$0 != .sort})
            self.sync(cloudSort: _sortInfo, direction: direction)
        }
        
        for sync in toSync {
            switch sync {
            case .favorite:
                self.sync(cloudFavortie: [:], direction: .toCloud)
            case .sort:
                self.sync(cloudSort: [:], direction: .toCloud)
            }
        }
    }
    /**
     Determin the sync direction based on local and cloud last update time stamp
     - parameter cloudLastUpdated: cloud last update time stamp
     - parameter localLastUpdated: local last update time stamp
     */
    func syncType(cloudLastUpdated: Int64, localLastUpdated: Int64) -> SyncDirection {
        let diff = localLastUpdated - cloudLastUpdated
        if diff == 0 {
            return .none
        } else if diff > 0 {
            return .toCloud
        } else {
            return .fromCloud
        }
    }
}

// MARK: - Sync function
fileprivate extension CloudSync {
    /**
     Sync favorite settings based on the sync direction
     - parameter cloudFavortie: cloud content
     - parameter direction: sync direction
     */
    func sync(cloudFavortie: [String: Any], direction: SyncDirection) {
        switch direction {
        case .toCloud:
            
            let params = FavoriteManager.shared.favoriteInfo.toDictionary(forCloud: true)
            self.upload(data: params, type: .favorite)
            
        case .fromCloud:
            let newFavorite = FavoriteContainer.init(fromDictionary: cloudFavortie)
            FavoriteManager.shared.update(favoriteInfo: newFavorite)
            
        default:
            break
        }
    }
    /**
     Sync sort settings based on the sync direction
     - parameter cloudSort: cloud content
     - parameter direction: sync direction
     */
    func sync(cloudSort: [String: Any], direction: SyncDirection) {
        switch direction {
        case .toCloud:
            
            let params = ChannelSorter.shared.sortInfo.toDictionary()
            self.upload(data: params, type: .sort)
            
        case .fromCloud:
            
            let newSortInfo = SortInfo.init(fromDictionary: cloudSort)
            ChannelSorter.shared.update(sortInfo: newSortInfo)
            
        default:
            break
        }
    }
}

// MARK: - Cloud Pusher
fileprivate extension CloudSync {
    func upload(data: [String: Any], type: DataType) {
        if AuthenticationService().isLoggedIn() {
            let reference = FireBaseSingleRequest.FireBaseReference.Users.child(FIRAuth.auth()!.currentUser!.uid).child(type.rawValue)
            reference.setValue(data, withCompletionBlock: { (error, _) in
                Logger.shared.log.debug("Sync result: \ntype: \(type.rawValue) \n\(error==nil ? "SYCNED" : error!.localizedDescription)")
            })
        }
    }
}
