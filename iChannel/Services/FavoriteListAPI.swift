//
//  FavoriteListAPI.swift
//  iChannel
//
//  Created by Danial on 16/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation

class FavoriteListAPI: Cancellable {
    typealias Completion = (_ channels: [ChannelInfo]?, _ error: Error?) -> ()
    fileprivate(set) var requestCollection: [WebRequest] = []
    /**
     Fetch contents of favorite channels
     - parameter channels: an array of minimal channel list
     - parameter preLoaded: current loaded channel list to implement lazy loading
     - parameter completion: async call back with the fetch result
     */
    func fetch(channels: [Channel],
               preLoaded: [ChannelInfo] = [],
               completion: @escaping Completion) {
        
        let newItems = channels.difference((preLoaded as [Channel])) // new channels to fetch
        if newItems.count > 0 {
            
            ChannelListAPI().fetch(channels: newItems, completion: { (channels, _, error) in
                if let _fetchedList = channels {
                    completion(preLoaded.union(_fetchedList), error)
                } else {
                    completion(preLoaded, error)
                }
                
            })
            
        } else { // channels changed, and remaining are already loaded - lazily return
            
            var _channels = [ChannelInfo]()
            for channel in preLoaded {
                if channels.contains(channel) {
                    _channels.append(channel)
                }
            }
            
            completion(ChannelSorter.shared.sort(channels: _channels) as? [ChannelInfo], nil)
        }
        
    }

    func cancellAll() {
        self.requestCollection.forEach({$0.cancel()})
    }
}
