//
//  FireBaseSingleRequest.swift
//  iChannel
//
//  Created by Danial on 18/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation
import Firebase

typealias GenericCompletion = (_ success: Bool, _ error: Error?) -> ()

class FireBaseSingleRequest {
	typealias SingleRequestCallBack = (_ snapshot: FIRDataSnapshot?, _ error: Error?) -> Void
    
	struct FireBaseReference {
		static var Users = FIRDatabase.database().reference().child("users")
	}

    /**
     Genericly handle firebase single event request and return the raw data
     - parameter reference: reference to the firebase data
     - parameter completion: async call back with the fetch result
     */
	class func handleReadRequestWithReference(reference: FIRDatabaseReference, completion: @escaping SingleRequestCallBack) {
        
        reference.observeSingleEvent(of: .value, with: { (snapshot) in
            
            completion(snapshot, nil)
            
        }) { (error) in
            Logger.shared.log.error("⛔ Requesting reference '\(reference)' failed: " + error.localizedDescription)
             completion(nil, error)
        }
	}
}
