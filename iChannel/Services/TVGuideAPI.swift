//
//  TVGuideAPI.swift
//  iChannel
//
//  Created by Danial on 17/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation
import PromiseKit

class TVGuideAPI: Cancellable {
    typealias Completion = (_ events: [EventContainer]?, _ maxPage: Int, _ error: Error?) -> ()
    var requestCollection: [WebRequest] = []
    fileprivate let channelListAPI = ChannelListAPI()
    fileprivate var maxPage: Int = 0
    fileprivate var fetched: [ChannelInfo] = []
    
    /**
     Fetch tv programm/events using page number (pagination)
     - parameter page: page number - if invalid completion method will return error
     - parameter date: date for the content period
     - parameter completion: async call back with the fetch result
     */
    func fetch(page: Int, date: Date, completion: @escaping Completion) {
        
        self.fetch(page: page).then { (fetchInfo) -> Promise<[Event]> in
            
            self.maxPage = fetchInfo.maxPage
            if self.maxPage < page || page == 0 {
                return Promise {fulfill, reject in reject(NSError.generateErrorFor(description: "page not found"))}
            } else {
                self.fetched = fetchInfo.channels.flatMap({$0})
                return self.fetchEventsFor(channels: fetchInfo.channels, date: date)
            }
            
        }.then { (events) -> Promise<[EventContainer]> in
            
            return self.group(events: events)
            
        }.then { (groupedChannels) -> () in
            
            completion(groupedChannels, self.maxPage, nil)
            
        }.catch { (error) in
            
            completion(nil, self.maxPage, error)
        }
    
    }

    func cancellAll() {
        self.requestCollection.forEach({$0.cancel()})
    }
}

fileprivate extension TVGuideAPI {
    
    struct ChannelFetch {
        var channels: [ChannelInfo]!
        var maxPage: Int! = 0
    }
    
    func fetch(page: Int) -> Promise<ChannelFetch> {
        return Promise { fulfill, reject in
            
            self.channelListAPI.fetch(page: page, completion: { (channels, maxPage, error) in
                if let _channels = channels {
                    fulfill(ChannelFetch.init(channels: _channels, maxPage: maxPage))
                } else {
                    reject(error ?? NSError.Generic())
                }
            })
        }
    }
    
    func fetchEventsFor(channels: [ChannelInfo], date: Date) -> Promise<[Event]> {
        return Promise { fulfill, reject in
            
            let slotInfo = EventSlotMaker.paramsFor(date: date)

            
            let params: [String: Any] = ["periodStart": slotInfo.from,
                                         "periodEnd": slotInfo.to,
                                         "channelId": channels.map({String($0.channelId)}).joined(separator: ",")]
            
            let request = WebRequest.init(path: APIPath.eventList, parameters: params, shouldCache: true)
            
            WebRequester.execute(request: request, completion: { (json, error) in
                
                if let _json = json, let _events = _json["getevent"].array {
                    
                    let _list = _events.flatMap({Event.init(fromJson: $0)})
                    let _sorted = _list.sorted(by: {$0.0.date < $0.1.date})

                    fulfill(_sorted)
                    
                } else {
                    
                     reject(error ?? NSError.Generic())
                }
            })
            
        }
    }
    
    func group(events: [Event]) -> Promise<[EventContainer]> {
        return Promise { fulfill, reject in
            
            let _grouped = events.group(by: {$0 as ChannelInfo})
            let _events = _grouped.flatMap({EventContainer.init(channel: $0.key, events: $0.value)})
            let _sorted = ChannelSorter.shared.sort(events: _events)
            for channelInfo in self.fetched {
                if let index = _sorted.index(where: {$0.channel == channelInfo}) {
                    _sorted[index].channel.set(thumbnailUrl: channelInfo.thumbnailUrl)
                }
            }
            fulfill(_sorted)
            
        }
    }
}
