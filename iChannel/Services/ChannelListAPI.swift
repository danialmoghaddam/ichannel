//
//  ChannelListAPI.swift
//  iChannel
//
//  Created by Danial on 14/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation
import PromiseKit
import SwiftyJSON

class ChannelListAPI: Cancellable {
    
    typealias Completion = (_ channels: [ChannelInfo]?, _ maxPage: Int, _ error: Error?) -> ()
    fileprivate(set) var requestCollection: [WebRequest] = []
    private var maxPage: Int = 0
    
    /**
     Fetch channels using page number (pagination)
     - parameter page: page number - if invalid completion method will return error
     - parameter pageSize: page size - default to 15
     - parameter completion: async call back with the fetch result
     */
    func fetch(page: Int,
               pageSize: Int = Config.defaultPageSize,
               completion: @escaping Completion) {
        
        self.getMinimalList().then { (minimalChannels) -> Promise<[ChannelInfo]?> in
            
            let pages = minimalChannels.chunk(pageSize)
            self.maxPage = pages.count
            
            if page > pages.count || page == 0 {
                return Promise {fulfill, reject in reject(NSError.generateErrorFor(description: "page not found"))}
            } else {
                return self.getList(for: pages[page-1]) //page - 1 to get the logical page starting at index 0
            }
            
        }.then { (channelList) -> () in
            
            completion(channelList, self.maxPage, nil)
            
        }.catch { (error) in
            
            completion(nil, self.maxPage, error)
        }
    }
    /**
     Fetch channels using a minimal channel list
     - parameter completion: async call back with the fetch result
     */
    func fetch(channels: [Channel],
               completion: @escaping Completion) {
        
        self.getList(for: channels).then { (channelList) -> () in
            
            completion(channelList, channelList?.count ?? 0, nil)
            
        }.catch { (error) in
            
            completion(nil, 0, error)
        }
        
    }
    
    func cancellAll() {
        self.requestCollection.forEach({$0.cancel()})
    }
}

fileprivate extension ChannelListAPI {
    func collect(request: WebRequest) {
        self.requestCollection.append(request)
    }
}

private extension ChannelListAPI {
    
    func getMinimalList() -> Promise<[Channel]> {
        return Promise { fulfill, reject in
            
            let request = WebRequest.init(path: APIPath.minimalChannelList, parameters: nil, shouldCache: true)
            
            WebRequester.execute(request: request, completion: { (json, error) in
                
                if let _json = json, let _channels = _json["channels"].array {
                    
                    let list = _channels.flatMap({Channel.init(fromJson: $0)})
                    let sorted = ChannelSorter.shared.sort(channels: list)
                    fulfill(sorted)
                    
                } else {
                    
                    reject(error ?? NSError.Generic())
                }
                
            })
            
            self.collect(request: request)
        }
    }
    
    func getList(`for` channels: [Channel]) -> Promise<[ChannelInfo]?> {
        return Promise { fulfill, reject in
            
            let channelIds: [Int] = channels.flatMap({$0.channelId})
            
            let params: [String: Any] = ["channelId": channelIds.map({String($0)}).joined(separator: ",")]
            let request = WebRequest.init(path: APIPath.channelList, parameters: params, shouldCache: true)
            
            WebRequester.execute(request: request, completion: { (json, error) in
                
                if let _json = json, let _channels = _json["channel"].array {
                    let list = _channels.flatMap({ChannelInfo.init(fromJson: $0)})
                    let sorted = ChannelSorter.shared.sort(channels: list) as? [ChannelInfo]
                    fulfill(sorted)
                    
                } else {
                    
                    reject(error ?? NSError.Generic())
                }
                
            })
            
            self.collect(request: request)
        }
    }
    
}
