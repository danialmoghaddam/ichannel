//
//  FavoriteTVC.swift
//  iChannel
//
//  Created by Danial on 16/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import UIKit

class FavoriteTVC: ChannelListTVC {

    fileprivate var favoriteAPI = FavoriteListAPI()
    fileprivate var favoriteList = [ChannelInfo]()
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Favorites"
        self.tableView.tableFooterView = UIView.init(frame: .zero)
        self.tableView.register(UINib(nibName: "ChannelCell", bundle: nil), forCellReuseIdentifier: ChannelCell.identifier)
        
        let _ = FavoriteManager.shared.favoriteInfo.observable.subscribe(onNext:{ _ in
            self.fetch(refresh: true)
        }).addDisposableTo(self.bag)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Setup Empty datasource
    override func setupEmptySource() {
        let data = EmptyListingDataSource.EmptyDisplayData.init(title: "Favorites",
                                                                description: "Your favorite list is empty. To add favorite a channel, tap on the heart shape from channel list",
                                                                image: UIImage(named: "favorite-empty")!)
        self.emptyDataSource = EmptyListingDataSource.init(displayData: data)
    }
    
    // MARK: - Fetch methods
    override func fetch(refresh: Bool) {
        self.emptyDataSource.isLoading = true
        let favorites = FavoriteManager.shared.favorites()
        self.favoriteAPI.fetch(channels: favorites, preLoaded: self.favoriteList) { [weak self] (channels, error) in
            guard let `self` = self else { return }
            self.emptyDataSource.isLoading = false
            self.updateContent(channels: channels)
        }
    }
    
    override func updateContent(channels: [ChannelInfo]?) {
        if let _channels = channels {
            self.favoriteList = _channels
        }
        self.tableView.reloadAnimated()
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.favoriteList.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ChannelCell.identifier, for: indexPath) as! ChannelCell
        cell.setup(content: self.favoriteList[indexPath.row])
        return cell
    }
}
