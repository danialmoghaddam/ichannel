//
//  EmptyListingDataSource.swift
//  iChannel
//
//  Created by Danial on 17/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation
import DZNEmptyDataSet

class EmptyListingDataSource: NSObject, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    

    struct EmptyDisplayData {
        private(set) var title: String!
        private(set) var desc: String!
        private(set) var image: UIImage!
        private(set) var buttonText: String!
        init(title: String, description: String, image: UIImage, buttonText: String = "") {
            self.title = title
            self.desc = description
            self.image = image
            self.buttonText = buttonText
        }
    }
    var isLoading: Bool = false
    private var displayData: EmptyDisplayData!
    private var tapCallBack: (() -> ())?
    
    required init(displayData: EmptyDisplayData, tapCallBack: (() -> ())? = nil) {
        self.displayData = displayData
        self.tapCallBack = tapCallBack
    }
    
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let attr = NSAttributedString.init(string: self.displayData.title, attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        return attr
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let attr = NSAttributedString.init(string: self.displayData.desc, attributes: nil)
        return attr
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return self.displayData.image
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> NSAttributedString! {
        let attr = NSAttributedString.init(string: self.displayData.buttonText, attributes: nil)
        return attr
    }
    
    //MARK: - Delegates
    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        self.tapCallBack?()
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        return !self.isLoading
    }
}
