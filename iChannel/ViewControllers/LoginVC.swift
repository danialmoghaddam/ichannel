//
//  LoginVC.swift
//  iChannel
//
//  Created by Danial on 18/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import UIKit
import GoogleSignIn
import SVProgressHUD

class LoginVC: UIViewController {

    fileprivate lazy var authService = AuthenticationService()
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Button actions
    @IBAction func googleTapped(_ sender: Any) {
        self.showLoading()
        self.authService.googleSignIn(uiDelegate: self) { [weak self] (loggedIn, error) in
            guard let `self` = self else { return }
            self.hideLoading()
            self.handleSingIn(success: loggedIn, error: error)
        }
    }
    
    @IBAction func continueTapped(_ sender: Any) {
        self.dismissVC(completion: nil)
    }
    
}
// MARK: - Login Handler
fileprivate extension LoginVC {
    func handleSingIn(success: Bool, error: Error?) {
        if success {
            SVProgressHUD.showSuccess(withStatus: "You are logged in")
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                self.dismissVC(completion: {
                    SVProgressHUD.dismiss()
                    CloudSync.shared.syncIfNeeded()
                })
            })
        } else if let _error = error {
            if (_error as NSError).code != -5 { // Don't show error when user canceled the sign-in flow
                self.showOKAlert(title: "Sign-In Failed", message: _error.localizedDescription)
            }
        }
    }
}

// MARK: - Login login delegate
extension LoginVC: GIDSignInUIDelegate {
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        viewController.dismiss(animated: true, completion: nil)
        self.showLoading()
    }
}

