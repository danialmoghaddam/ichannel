//
//  ChannelListTVC.swift
//  iChannel
//
//  Created by Danial on 15/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import UIKit
import RxSwift
import Presentr

class ChannelListTVC: UITableViewController, SortPresenter, LoginPresenter {
    
    @IBOutlet weak var sortBtn: UIBarButtonItem!
    @IBOutlet weak var reloadBtn: UIBarButtonItem!
    
    var bag = DisposeBag()
    fileprivate var channelList = [ChannelInfo]()
    fileprivate var maxPage: Int = 0
    fileprivate var currentPage: Int = 0
    fileprivate var canLoadMore: Bool {
        return currentPage < maxPage
    }
    fileprivate var channelAPI = ChannelListAPI()
    internal var emptyDataSource: EmptyListingDataSource!
    var presenter: Presentr {
        get {
            let presenter = Presentr(presentationType: .topHalf)
            presenter.transitionType = TransitionType.coverVerticalFromTop
            return presenter
        }
    }
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Channels"
        
        self.updateSortBtn()
        self.setupEmptySource()
        self.emptyDataSource.isLoading = true
        
        self.tableView.emptyDataSetSource = self.emptyDataSource
        self.tableView.emptyDataSetDelegate = self.emptyDataSource
        self.tableView.tableFooterView = UIView.init(frame: .zero)
        self.tableView.register(UINib(nibName: "ChannelCell", bundle: nil), forCellReuseIdentifier: ChannelCell.identifier)
        
        self.fetch(refresh: true)
        
        let _ = ChannelSorter.shared.sortInfo.observable.subscribe(onNext:{ _ in
            self.fetch(refresh: true)
            self.updateSortBtn()
        }).addDisposableTo(self.bag)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Setup Empty datasource
    func setupEmptySource() {
        let data = EmptyListingDataSource.EmptyDisplayData.init(title: "Channels", description: "There was a problem loading the channel list.", image: UIImage(named: "tv")!, buttonText: "Reload")
        self.emptyDataSource = EmptyListingDataSource.init(displayData: data, tapCallBack: { [weak self] in
            self?.fetch(refresh: true)
        })
    }
    
    // MARK: - Fetch methods
    func fetch(refresh: Bool = false) {
        self.emptyDataSource.isLoading = true
        
        if refresh {
            Logger.shared.log.debug("Refreshing ...")
            self.channelAPI.cancellAll()
            self.channelList = []
            self.maxPage = 0
            self.currentPage = 0
            self.tableView.reloadAnimated()
            self.showLoading()
        }
        
        self.currentPage += 1
        
        self.channelAPI.fetch(page: self.currentPage) { [weak self] (channels, maxPage, error) in
            guard let `self` = self else { return }
            self.emptyDataSource.isLoading = false
            self.maxPage = maxPage
            self.updateContent(channels: channels)
        }
    }
    
    func updateContent(channels: [ChannelInfo]?) {
        
        self.hideLoading()
        
        if self.channelList.count == 0 , let _channels = channels {
            
            self.channelList = _channels
            self.tableView.reloadAnimated()
            
        } else if let _channels = channels {
            
            var indexPaths : [IndexPath] = []
            for index in 0..._channels.count - 1 {
                indexPaths.append(IndexPath.init(row: index + self.channelList.count , section: 0))
            }
            
            self.channelList.append(contentsOf: _channels)
            self.tableView.beginUpdates()
            self.tableView.insertRows(at: indexPaths, with: .fade)
            if !canLoadMore {
                self.tableView.deleteRows(at: [IndexPath.init(row: 0, section: 1)], with: .automatic)
            }
            self.tableView.endUpdates()
            self.tableView.scrollToRow(at: indexPaths.first!, at: .none, animated: true)
            
        } else {
            
            self.tableView.reloadAnimated()
        }

    }
    
    // MARK: - Button Actions
    @IBAction func reloadTapped(_ sender: UIBarButtonItem) {
        if Reachability.shared.isReachable {
            self.fetch(refresh: true)
        }
    }
    
    @IBAction func sortTapped(_ sender: Any) {
        self.presetSortPicker(from: self)
    }
    
    private func updateSortBtn() {
        switch ChannelSorter.shared.sortInfo.type {
        case .number:
            self.sortBtn.image = UIImage(named: "numeric-sort")
        default:
            self.sortBtn.image = UIImage(named: "alphanumeric-sort")
        }
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return canLoadMore ? 1 : 0
        } else {
            return self.channelList.count
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.cell(for: indexPath)
    }
    
    // MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 1 && self.canLoadMore && self.channelList.count > 0 {
            self.fetch()
        }
    }
}

// MARK: - Cell Factory
fileprivate extension ChannelListTVC {
    func cell(`for` indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
            
        case 0:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: ChannelCell.identifier, for: indexPath) as! ChannelCell
            cell.setup(content: self.channelList[indexPath.row])
            cell.didChangeFavorite = { [weak self] in
                guard let `self` = self else { return }
                self.presentLoginIfNeeded(force: false, from: self)
            }
            return cell
            
        case 1:
            
            return tableView.dequeueReusableCell(withIdentifier: "LoadingCell", for: indexPath)
            
        default:
            assertionFailure("Invalid section to generate cell for")
        }
        
        return UITableViewCell()
    }
}
