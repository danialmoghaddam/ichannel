//
//  MainTabVC.swift
//  iChannel
//
//  Created by Danial on 19/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import UIKit

class MainTabVC: UITabBarController, UITabBarControllerDelegate {

    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Tab bar delegate
    public func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if let identifier = viewController.restorationIdentifier, identifier == "ProfileNVC", !AuthenticationService().isLoggedIn() {
            self.performSegue(withIdentifier: "showLogin", sender: nil)
            return false
        }
        
        return true
    }
}
