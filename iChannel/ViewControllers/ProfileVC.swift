//
//  ProfileVC.swift
//  iChannel
//
//  Created by Danial on 19/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import UIKit
import FirebaseAuth

class ProfileVC: UIViewController {

    @IBOutlet weak var userEmailLbl: UILabel!
    
    fileprivate let auth = AuthenticationService()
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.auth.isLoggedIn() {
            self.userEmailLbl.text = "Logged in as: \(FIRAuth.auth()!.currentUser!.email ?? "Unknow")"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Button actions
    @IBAction func logoutTapped(_ sender: Any) {
        
        if self.auth.isLoggedIn() {
            self.auth.logout()
            PersistentManager.purge()
            self.navigationController?.tabBarController?.selectedIndex = 0
        }
    }
}
