//
//  SortVC.swift
//  iChannel
//
//  Created by Danial on 18/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import UIKit

class SortVC: UIViewController {

    
    @IBOutlet weak var favSwitch: UISwitch!
    @IBOutlet weak var sortSegment: UISegmentedControl!
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        switch ChannelSorter.shared.sortInfo.type {
        case .name:
            self.sortSegment.selectedSegmentIndex = 0
        case .number:
            self.sortSegment.selectedSegmentIndex = 1
        }
        
        self.favSwitch.isOn = AuthenticationService().isLoggedIn() ? ChannelSorter.shared.sortInfo.favorite : false
        self.favSwitch.isEnabled = AuthenticationService().isLoggedIn()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Button Actions
    @IBAction func sortChannelChanged(_ sender: UISegmentedControl) {
        ChannelSorter.shared.update(sortType: sender.selectedSegmentIndex == 0 ? .name : .number)
    }
    
    @IBAction func favSWTapped(_ sender: UISwitch) {
        ChannelSorter.shared.update(favorite: sender.isOn)
    }
    
    
    @IBAction func doneTapped(_ sender: Any) {
        self.dismissVC(completion: nil)
    }
}
