//
//  TVGuideTVC.swift
//  iChannel
//
//  Created by Danial on 17/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import UIKit
import RxSwift
import Presentr

class TVGuideTVC: UITableViewController, SortPresenter {

    @IBOutlet weak var sortBtn: UIBarButtonItem!
    
    fileprivate lazy var tvGuideAPI = TVGuideAPI()
    fileprivate var events = [EventContainer]()
    fileprivate var maxPage: Int = 0
    fileprivate var currentPage: Int = 0
    fileprivate var canLoadMore: Bool {
        return currentPage < maxPage
    }
    fileprivate var bag = DisposeBag()
    fileprivate var refreshTimer: Timer?
    fileprivate var isLoading: Bool = false
    var presenter: Presentr {
        get {
            let presenter = Presentr(presentationType: .topHalf)
            presenter.transitionType = TransitionType.coverVerticalFromTop
            return presenter
        }
    }
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.updateSortBtn()
        
        self.tableView.tableFooterView = UIView.init(frame: .zero)
        
        let _ = ChannelSorter.shared.sortInfo.observable.subscribe(onNext:{ _ in
            self.fetch(refresh: true)
            self.updateSortBtn()
        }).addDisposableTo(self.bag)
        
        self.fetch(refresh: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.isLoading { self.showLoading() }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Fetch methods
    func fetch(refresh: Bool = false) {
        self.isLoading = true
        if refresh {
            self.events = []
            self.maxPage = 0
            self.currentPage = 0
            self.tableView.reloadAnimated()
            self.refreshTimer?.invalidate()
            self.refreshTimer = Timer(fireAt: Date().tomorrow.startOfDay, interval: 0, target: self, selector: #selector(refreshForNewDay(_:)), userInfo: nil, repeats: false)
            RunLoop.main.add(self.refreshTimer!, forMode: RunLoopMode.commonModes)
            self.showLoading()
        }
        
        self.currentPage += 1
        
        self.tvGuideAPI.fetch(page: self.currentPage, date: Date()) { [weak self] (events, maxPage, error) in
            guard let `self` = self else { return }
            self.maxPage = maxPage
            self.updateContent(events: events)
        }
    }
    
    func refreshForNewDay(_ timer: Timer) {
        self.fetch(refresh: true)
    }
    
    func updateContent(events: [EventContainer]?) {
        self.isLoading = false
        self.hideLoading()
        if self.events.count == 0 , let _events = events {
            self.events = _events
            self.tableView.reloadAnimated()
            
        } else if let _events = events {
            
            var indexPaths : [IndexPath] = []
            for index in 0..._events.count - 1 {
                indexPaths.append(IndexPath.init(row: index + self.events.count , section: 0))
            }
            
            self.events.append(contentsOf: _events)
            self.tableView.beginUpdates()
            self.tableView.insertRows(at: indexPaths, with: .fade)
            if !canLoadMore {
                self.tableView.deleteRows(at: [IndexPath.init(row: 0, section: 1)], with: .automatic)
            }
            self.tableView.endUpdates()
            self.tableView.scrollToRow(at: indexPaths.first!, at: .none, animated: true)
            
        } else {
            
            self.tableView.reloadAnimated()
        }

    }
    
    // MARK: - Button actions
    @IBAction func sortTapped(_ sender: Any) {
        self.presetSortPicker(from: self)
    }
    
    private func updateSortBtn() {
        switch ChannelSorter.shared.sortInfo.type {
        case .number:
            self.sortBtn.image = UIImage(named: "numeric-sort")
        default:
            self.sortBtn.image = UIImage(named: "alphanumeric-sort")
        }
    }
    
    @IBAction func refreshTapped(_ sender: Any) {
        self.fetch(refresh: true)
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return canLoadMore ? 1 : 0
        } else {
            return self.events.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.cell(for: indexPath)
    }

    // MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 1 && self.canLoadMore && self.events.count > 0 {
            self.fetch()
        }
    }
}


// MARK: - Cell Factory
fileprivate extension TVGuideTVC {
    func cell(`for` indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
            
        case 0:
            
            let cell = self.tableView.dequeueReusableCell(withIdentifier: ChannelEventCell.identifier, for: indexPath) as! ChannelEventCell
            cell.setup(content: self.events[indexPath.row])
            return cell
            
        case 1:
            
            return self.tableView.dequeueReusableCell(withIdentifier: "LoadingCell", for: indexPath)
            
        default:
            assertionFailure("Invalid section to generate cell for")
        }
        
        return UITableViewCell()
    }
}
