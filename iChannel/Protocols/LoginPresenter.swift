//
//  LoginPresenter.swift
//  iChannel
//
//  Created by Danial on 18/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation
import UIKit

protocol LoginPresenter {
    func presentLoginIfNeeded(force: Bool, from: UIViewController)
}

extension LoginPresenter {
    func presentLoginIfNeeded(force: Bool, from: UIViewController) {
        if force {
            self.present(from: from)
        } else if !AuthenticationService().isLoggedIn() {
            self.present(from: from)
        }
    }
}

fileprivate extension LoginPresenter {
    func present(from: UIViewController) {
        let loginNVC = UIStoryboard.init(name: "SignIn", bundle: nil).instantiateViewController(withIdentifier: "LoginNVC")
        from.presentVC(loginNVC)
    }
}
