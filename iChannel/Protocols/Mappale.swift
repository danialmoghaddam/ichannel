//
//  Parsable.swift
//  iChannel
//
//  Created by Danial on 14/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol Mappale {
    /**
     Required init method - It will return nill if the content validation fails
     - parameter json: a json object containing the data to parse
     */
    init?(fromJson json: JSON!)
}
