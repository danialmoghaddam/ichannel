//
//  SetupableCell.swift
//  iChannel
//
//  Created by Danial on 15/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation

protocol SetupableCell {
    func setup<T>(content: T)
    static var identifier: String! {get}
}
