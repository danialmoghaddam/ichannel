//
//  SortPresenter.swift
//  iChannel
//
//  Created by Danial on 18/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation
import Presentr

protocol SortPresenter {
    var presenter: Presentr {get}
    func presetSortPicker(from: UIViewController)
}

extension SortPresenter {
    func presetSortPicker(from: UIViewController) {
        let sotryBoard = UIStoryboard.init(name: "Sort", bundle: nil)
        if let sortVC = sotryBoard.instantiateViewController(withIdentifier: "SortVC") as? SortVC {
            from.customPresentViewController(self.presenter, viewController: sortVC, animated: true, completion: nil)
        }
    }
}
