//
//  APIProtocol.swift
//  iChannel
//
//  Created by Danial on 14/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation

protocol Cancellable {
    /**
     Request collector to collect all the invoked request for later if need to cancel them
     */
    var requestCollection: [WebRequest] {get}
    /**
     Cancell all the ongoing requests
     */
    func cancellAll()
}
