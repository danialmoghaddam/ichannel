//
//  APIPath.swift
//  iChannel
//
//  Created by Danial on 14/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation

class APIPath {
    
    // BASE URL
    #if DEBUG
    static let hostUrlString = "https://ams-api.astro.com.my"
    #else
    static let hostUrlString = "https://ams-api.astro.com.my" //Production base url
    #endif
    
    static let apiVersion = "/ams/v3/"
    
    // API end points
    static let minimalChannelList: String = "getChannelList"
    static let channelList: String = "getChannels"
    static let eventList: String = "getEvents"
    
}
