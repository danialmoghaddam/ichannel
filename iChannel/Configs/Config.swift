//
//  Config.swift
//  iChannel
//
//  Created by Danial on 14/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation

class Config {
    
    static let maxMemoryCacheSize: Int = 50 * (1024 * 1024)
    static let defaultPageSize: Int = 15
    static let timeStampMaxFloat: Int = 2
    
}
