//
//  DateFormatExtension.swift
//  iChannel
//
//  Created by Danial on 17/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation

extension DateFormatter {
    class var `default`: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.s"
        return formatter
    }
    
    class var utc: DateFormatter {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.init(identifier: "UTC")!
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.s"
        return formatter
    }
    
    class var timeOnly: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        return formatter
    }
    
    class var shortTime: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        return formatter
    }
    
}
