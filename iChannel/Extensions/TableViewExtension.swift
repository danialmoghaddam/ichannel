//
//  TableViewExtension.swift
//  iChannel
//
//  Created by Danial on 15/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    func reloadAnimated() {
        
        UIView.transition(with: self,
                          duration: 0.45,
                          options: .transitionCrossDissolve,
                          animations: { () -> Void in
                            
                            self.reloadData()
                            
        }, completion: nil)
    }
}
