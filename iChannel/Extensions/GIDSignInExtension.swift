//
//  GIDSignInExtension.swift
//  iChannel
//
//  Created by Danial on 18/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation
import GoogleSignIn
import PromiseKit

extension GIDSignIn {
    public func promise() -> Promise<Any> {
        let proxy = AuthenticationService()
        delegate = proxy
        proxy.retainCycle = proxy
        self.signIn()
        return proxy.promise
    }
}
