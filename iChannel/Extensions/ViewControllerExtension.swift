//
//  ViewControllerExtension.swift
//  iChannel
//
//  Created by Danial on 18/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
import Presentr

extension UIViewController {
    func showLoading() {
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
    }
    
    func hideLoading() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    
    typealias OKTapped = () -> ()
    
    func showOKAlert(title: String, message: String, okTapped: OKTapped? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
            
            okTapped?()
            
        }))
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
}
