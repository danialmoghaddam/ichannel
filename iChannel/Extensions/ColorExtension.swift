//
//  ColorExtension.swift
//  iChannel
//
//  Created by Danial on 15/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation
import UIKit
import EZSwiftExtensions

extension UIColor {
    class var themeColor: UIColor {
        return UIColor(hexString: "#f442bc")!
    }
    
    class var themeTintColor: UIColor {
        return .white
    }
}
