//
//  DateExtension.swift
//  iChannel
//
//  Created by Danial on 17/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation


extension Date {
    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }
    
    var endOfDay: Date? {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfDay)
    }
    
    var tomorrow: Date {
        var components = DateComponents()
        components.day = 1
        return Calendar.current.date(byAdding: components, to: self)!
    }
    
    func forward(hours: Int) -> Date? {
        var components = DateComponents()
        components.hour = hours
        return Calendar.current.date(byAdding: components, to: self)
    }
    
    func forward(seconds: Int) -> Date! {
        var components = DateComponents()
        components.second = seconds
        return Calendar.current.date(byAdding: components, to: self)
    }
    
    func backward(hours: Int) -> Date? {
        var components = DateComponents()
        components.hour = -hours
        return Calendar.current.date(byAdding: components, to: self)
    }
}
