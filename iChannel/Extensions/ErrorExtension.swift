//
//  ErrorExtension.swift
//  iChannel
//
//  Created by Danial on 14/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation

extension NSError {
    
    class func Generic() -> NSError {
        return self.generateErrorFor(domain: "Server",
                                     description: "Something unexpected/unhandled happened!")
    }

    class func generateErrorFor(domain: String = "Inernal", description: String, code: Int = -1) -> NSError {
        return NSError(
            domain: domain,
            code: -1,
            userInfo: [
                NSLocalizedDescriptionKey : description
            ]
        )
    }
}
