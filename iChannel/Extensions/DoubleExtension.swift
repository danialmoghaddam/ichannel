//
//  DoubleExtension.swift
//  iChannel
//
//  Created by Danial on 19/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation

extension Double {
    /// Rounds the double to decimal places value
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
    func int64() -> Int64 {
        return Int64(self)
    }
}
