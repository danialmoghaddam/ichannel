//
//  StringExtension.swift
//  iChannel
//
//  Created by Danial on 17/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation

extension String {
    func minutesFromString() -> Int {
        var components: Array = self.components(separatedBy: ":")
        if components.count >= 2 {
            let hours = components[0].toInt()!
            let minutes = components[1].toInt()!
            return Int((hours * 60) + minutes)
        }
        return 0
    }
}
