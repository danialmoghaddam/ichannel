//
//  SortInfo.swift
//  iChannel
//
//  Created by Danial on 15/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation
import RxSwift

enum SortType: String {
    case name = "channelTitle"
    case number = "channelStbNumber"
}

struct SortInfo {
    var lastUpdate : Int64 = 0
    var type : SortType = .number {
        didSet {
            self.observable.onNext(true)
        }
    }
    var favorite: Bool = false {
        didSet {
            self.observable.onNext(true)
        }
    }
    var observable = PublishSubject<Bool>()
    
    init(fromDictionary dictionary: [String: Any]?) {
        
        if let _lastUpdate = dictionary?["lastUpdate"] as? Int64 {
            self.lastUpdate = _lastUpdate
        }
        
        if let _type = dictionary?["type"] as? String,
            let _sortType = SortType.init(rawValue: _type) {

            self.type = _sortType
        }
        
        if let _favorite = dictionary?["favorite"] as? Bool {
            self.favorite = _favorite
        }
    }
    
    func toDictionary() -> [String: Any] {
        var dictionary = [String: Any]()
        dictionary["lastUpdate"] = self.lastUpdate
        dictionary["type"] = self.type.rawValue
        dictionary["favorite"] = self.favorite
        return dictionary
    }
}
