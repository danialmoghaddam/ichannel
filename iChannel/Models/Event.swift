//
//  Event.swift
//  iChannel
//
//  Created by Danial on 17/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation
import SwiftyJSON

class Event: ChannelInfo {
    private(set) var program: String!
    private(set) var date: Date!
    var endDate: Date {
        return date.addingTimeInterval(Double(duration) * 60)
    }
    private(set) var duration: Int!
    
    required init?(fromJson json: JSON!) {
        super.init(fromJson: json)
        if json.isEmpty ||
            json["displayDateTimeUtc"].stringValue.isEmpty ||
            json["displayDuration"].stringValue.isEmpty {
            return nil
        }
        
        self.program = json["programmeTitle"].stringValue
        self.date = DateFormatter.utc.date(from: json["displayDateTimeUtc"].stringValue)
        self.duration = json["displayDuration"].stringValue.minutesFromString()
        self.set(category: json["genre"].stringValue)
    }
}
