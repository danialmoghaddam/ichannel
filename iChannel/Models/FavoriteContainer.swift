//
//  Favorite.swift
//  iChannel
//
//  Created by Danial on 15/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation
import SwiftyJSON
import RxSwift

struct FavoriteContainer {
    var lastUpdate : Int64 = 0
    var channels: [Channel] = []
    var observable = PublishSubject<[Channel]>()
    
    init(fromDictionary dictionary: [String: Any]?) {
        if let _lastUpdate = dictionary?["lastUpdate"] as? Int64 {
            self.lastUpdate = _lastUpdate
        }
        
        if let _channelsData = dictionary?["channels"] as? [Data] { // from locale persistent
            
            let _channels = _channelsData.flatMap({JSON($0)}).flatMap({Channel.init(fromJson: $0)})
            self.channels = _channels
            
        } else if let _channelDic = dictionary?["channels"] as? [String: Any] { // from cloud data
            
            let _channels = _channelDic.flatMap({JSON($0.value)}).flatMap({Channel.init(fromJson: $0)})
            self.channels = _channels
        }
        
    }
    
    func toDictionary(forCloud: Bool = false) -> [String: Any] {
        var dictionary = [String: Any]()
        dictionary["lastUpdate"] = self.lastUpdate
        if forCloud {
            var _channels = [String: Any]()
            for channel in self.channels {
                if let dic = channel.toDictionary() {
                    _channels[String(channel.channelId)] = dic
                }
            }
            dictionary["channels"] = _channels
        } else {
            dictionary["channels"] = self.channels.flatMap({$0.persistable()})
        }
        
        return dictionary
    }
}
