//
//  ChannelAbstract.swift
//  iChannel
//
//  Created by Danial on 14/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation
import SwiftyJSON


class Channel: Mappale, Equatable, Hashable {
    var channelId : Int!
    var channelStbNumber : Int!
    var channelTitle : String!
    var isFavorite: Bool {
        return FavoriteManager.shared.isFavorite(channel: self)
    }
    private var json: JSON!
    
    
    required init?(fromJson json: JSON!) {
        //Discard channels which their channel numbers are <= 0
        if json.isEmpty || json["channelStbNumber"].intValue <= 0 {
            return nil
        }
        self.channelId = json["channelId"].intValue
        self.channelStbNumber = json["channelStbNumber"].intValue
        self.channelTitle = json["channelTitle"].stringValue
        
        self.json = json
    }
    
    var hashValue: Int {
        return channelId.hashValue ^ channelStbNumber.hashValue ^ channelTitle.hashValue
    }
    
    func persistable() -> Data? {
        return try! self.json.rawData()
    }
    
    func toDictionary() -> [String: Any]? {
        return ["channelId": self.channelId,
                "channelStbNumber": self.channelStbNumber,
                "channelTitle": self.channelTitle]
    }
}

func ==(lhs: Channel, rhs: Channel) -> Bool {
    return lhs.channelId == rhs.channelId
}
