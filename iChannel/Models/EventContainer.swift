//
//  EventContainer.swift
//  iChannel
//
//  Created by Danial on 18/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation

struct EventContainer: Equatable {
    var channel: ChannelInfo!
    var events: [Event]!
    
    init(channel: ChannelInfo, events: [Event]) {
        self.channel = channel
        self.events = events
    }
}

func ==(lhs: EventContainer, rhs: EventContainer) -> Bool {
    return lhs.channel.channelId == rhs.channel.channelId
}
