//
//  ChannelInfo.swift
//  iChannel
//
//  Created by Danial on 15/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation
import SwiftyJSON

class ChannelInfo: Channel {
    
    private(set) var language: String?
    private(set) var category: String?
    private(set) var thumbnailUrl: String?
    
    required init?(fromJson json: JSON!) {
        super.init(fromJson: json)
        if json.isEmpty {
            return nil
        }
        
        self.language = json["channelLanguage"].stringValue
        self.category = json["channelCategory"].stringValue
        if let logos = json["channelExtRef"].array {
            let flatMap = logos.flatMap({$0.dictionary}).filter({$0["subSystem"]?.stringValue.hasPrefix("Pos_100x62") ?? false})
            if let _asset = flatMap.first, let _thumbnail = _asset["value"]?.stringValue {
                self.set(thumbnailUrl: _thumbnail)
            }
        }
    }
    
    func set(thumbnailUrl: String?) {
        self.thumbnailUrl = thumbnailUrl
    }
    
    func set(category: String?) {
        self.category = category
    }
}
