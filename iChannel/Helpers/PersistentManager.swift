//
//  PersistentManager.swift
//  iChannel
//
//  Created by Danial on 15/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation

class PersistentManager {
    
    fileprivate static let sortInfoKey = "sortInfoKey"
    fileprivate static let favoriteInfoKey = "favoriteInfoKey"
    fileprivate static let loginShownKey = "loginShownKey"
    
    class var sortInfo: [String: Any]? {
        get {
            return UserDefaults.standard.dictionary(forKey: self.sortInfoKey)
        }
        
        set {
            UserDefaults.standard.set(newValue, forKey: self.sortInfoKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    class var favoriteInfo: [String: Any]? {
        get {
            return UserDefaults.standard.dictionary(forKey: favoriteInfoKey)
        }
        
        set {
            UserDefaults.standard.set(newValue, forKey: self.favoriteInfoKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    class func purge() {
        self.sortInfo = nil
        self.favoriteInfo = nil
        ChannelSorter.shared.reload()
        FavoriteManager.shared.reload()
    }
    
}
