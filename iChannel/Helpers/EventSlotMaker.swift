//
//  EventSlotMaker.swift
//  iChannel
//
//  Created by Danial on 17/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation
import EZSwiftExtensions

class EventSlotMaker {
    class func paramsFor(date: Date) -> (from: String, to: String) {
        let fromDate: Date = date.startOfDay
        let toDate: Date = date.endOfDay!
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        
        return (formatter.string(from: fromDate), formatter.string(from: toDate))
    }
}
