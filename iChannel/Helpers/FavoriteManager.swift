//
//  FavoriteManager.swift
//  iChannel
//
//  Created by Danial on 15/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation

class FavoriteManager {
    
    static let shared = FavoriteManager()
    fileprivate(set) var favoriteInfo: FavoriteContainer!
    
    private init() {
        self.favoriteInfo = FavoriteContainer.init(fromDictionary: PersistentManager.favoriteInfo)
    }
    /**
     Reload (reset) the favorite contents
     */
    func reload() {
        self.update(favoriteInfo: FavoriteContainer.init(fromDictionary: PersistentManager.favoriteInfo), ignoreTimeStamp: true)
    }
    
    func favorites() -> [Channel] {
        return self.favoriteInfo.channels
    }
    
    func isFavorite(channel: Channel) -> Bool {
        return self.favorites().contains(channel)
    }
    
    /**
     Update favorite using a new content
     - parameter favoriteInfo: new content to use
     - parameter ignoreTimeStamp: ignore the new content time stamp and proceed with updating
     */
    func update(favoriteInfo: FavoriteContainer, ignoreTimeStamp: Bool = false) {
        if favoriteInfo.lastUpdate > self.favoriteInfo.lastUpdate || ignoreTimeStamp {
            self.favoriteInfo.channels = favoriteInfo.channels
            self.favoriteInfo.lastUpdate = favoriteInfo.lastUpdate
            self.persist()
            self.favoriteInfo.observable.onNext(self.favoriteInfo.channels)
        }
    }
    /**
     Convenient method to add or remove a channel from favorite list
     - parameter channel: channel to add or remove
     */
    func addOrRemove(channel: Channel) {
        if self.isFavorite(channel: channel) {
            self.remove(channel: channel)
        } else {
            self.add(channel: channel)
        }
        
        self.favoriteInfo.observable.onNext(self.favoriteInfo.channels)
    }
    
    func add(channel: Channel) {
        if !self.isFavorite(channel: channel) {
            self.favoriteInfo.channels.append(channel)
            self.favoriteInfo.lastUpdate = Date().timeIntervalSince1970.int64()
            self.persist()
        }
    }
    
    func remove(channel: Channel) {
        self.favoriteInfo.channels.removeFirst(channel)
        self.favoriteInfo.channels = self.favorites().filter({$0.channelId != channel.channelId})
        self.favoriteInfo.lastUpdate = Date().timeIntervalSince1970.int64()
        self.persist()
    }
}

fileprivate extension FavoriteManager {
    func persist() {
        PersistentManager.favoriteInfo = self.favoriteInfo.toDictionary()
        Logger.shared.log.info("New sorting type is persisted: There are \(self.favoriteInfo.channels.count) favorite channels)")
    }
}
