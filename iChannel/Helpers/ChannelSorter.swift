//
//  ChannelSorter.swift
//  iChannel
//
//  Created by Danial on 15/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation

class ChannelSorter {
    static let shared = ChannelSorter()
    
    fileprivate(set) var sortInfo: SortInfo!
    
    private init() {
        self.sortInfo = SortInfo.init(fromDictionary: PersistentManager.sortInfo)
    }
    /**
     Reload (reset) the sort settings
     */
    func reload() {
        self.update(sortInfo: SortInfo.init(fromDictionary: PersistentManager.sortInfo), ignoreTimeStamp: true)
    }
    /**
     Convenient sort function to sort channel list using sort settings
     - parameter channels: channel list to be sorted
     - parameter isFav: if favorite should be on top of the list
     */
    func sort(channels: [Channel], isFav: Bool = false) -> [Channel] {
        
        var _sorted: [Channel]

        switch self.sortInfo.type {
        case .name:
            _sorted = channels.sorted(by: {$0.channelTitle.localizedCaseInsensitiveCompare($1.channelTitle) == ComparisonResult.orderedAscending})
        case .number:
            _sorted = channels.sorted(by: {$0.channelStbNumber < $1.channelStbNumber})
        }
        
        if self.sortInfo.favorite {
            return self.sortFavorite(channels: _sorted)
        }
        
        return _sorted
    }
    
    private func sortFavorite(channels: [Channel]) -> [Channel] {
        var favorites = channels.filter({$0.isFavorite == true})
        let nonFavorite = channels.difference(favorites)
        
        favorites.append(contentsOf: nonFavorite)
        return favorites
    }
    /**
     Convenient sort function to sort channel program list using sort settings
     - parameter events: channel list to be sorted
     */
    func sort(events: [EventContainer]) -> [EventContainer] {
        
        switch self.sortInfo.type {
        case .name:
            return events.sorted(by: {$0.0.channel.channelTitle.localizedCaseInsensitiveCompare($0.1.channel.channelTitle) == ComparisonResult.orderedAscending})
        case .number:
            return events.sorted(by: {$0.0.channel.channelStbNumber < $0.1.channel.channelStbNumber})
        }
    }
    /**
     Update sort setting using a new setting
     - parameter sortInfo: new settings to use
     - parameter ignoreTimeStamp: ignore the new content time stamp and proceed with updating
     */
    func update(sortInfo: SortInfo, ignoreTimeStamp: Bool = false) {
        if sortInfo.lastUpdate > self.sortInfo.lastUpdate  || ignoreTimeStamp {
            
            if sortInfo.type != self.sortInfo.type {
                self.sortInfo.type = sortInfo.type
            }
            if sortInfo.favorite != self.sortInfo.favorite {
                self.sortInfo.favorite = sortInfo.favorite
            }
            self.sortInfo.lastUpdate = sortInfo.lastUpdate
            self.persist()
        }
        
    }
    
    func update(favorite: Bool) {
        self.sortInfo.favorite = favorite
        self.sortInfo.lastUpdate = Date().timeIntervalSince1970.int64()
        self.persist()
    }
    
    func update(sortType: SortType) {
        self.sortInfo.type = sortType
        self.sortInfo.lastUpdate = Date().timeIntervalSince1970.int64()
        self.persist()
    }
    
    func toggle() {
        switch self.sortInfo.type {
        case .name:
            self.update(sortType: .number)
        case .number:
            self.update(sortType: .name)
        }
    }
}

fileprivate extension ChannelSorter {
    func persist() {
        
        PersistentManager.sortInfo = self.sortInfo.toDictionary()
        Logger.shared.log.info("New sorting type is persisted: \(self.sortInfo.toDictionary())")
    }
}
