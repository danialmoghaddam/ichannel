//
//  UITheme.swift
//  iChannel
//
//  Created by Danial on 15/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD

class UITheme {
    class func applyUIAppearance() {
        
        UINavigationBar.appearance().barTintColor  = UIColor.themeColor
        UINavigationBar.appearance().tintColor = UIColor.themeTintColor
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.themeTintColor]
        
        UITabBar.appearance().tintColor = UIColor.themeColor
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        SVProgressHUD.setDefaultMaskType(.none)
    }
}
