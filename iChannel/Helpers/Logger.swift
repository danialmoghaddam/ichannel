//
//  Logger.swift
//  iChannel
//
//  Created by Danial on 15/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import Foundation
import XCGLogger

class Logger {
    static let shared = Logger()
    let log = XCGLogger.default

    private init() {
        #if DEBUG
            self.log.setup(level: .verbose, showThreadName: true, showLevel: true, showFileNames: true, showLineNumbers: true)
        #else
            log.setup(level: .error, showThreadName: false, showLevel: false, showFileNames: false, showLineNumbers: false)
        #endif
    }
}
