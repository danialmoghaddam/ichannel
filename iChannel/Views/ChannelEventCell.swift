//
//  ChannelEventCell.swift
//  iChannel
//
//  Created by Danial on 18/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import UIKit
import Kingfisher

class ChannelEventCell: UITableViewCell, SetupableCell {
    
    static var identifier: String! = "ChannelEventCell"
    
    @IBOutlet weak var channelImage: UIImageView!
    @IBOutlet weak var channelNoLbl: UILabel!
    @IBOutlet weak var programLbl: UILabel!
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    
    fileprivate var imageDownloadTask: RetrieveImageTask?
    fileprivate var event: EventContainer?
    fileprivate var refreshTimer: Timer?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setup<T>(content: T) {
        
        self.imageView?.image = nil
        self.imageDownloadTask?.cancel()
        self.refreshTimer?.invalidate()
        
        if let _event = content as? EventContainer {
            self.event = _event
            
            if let _url = _event.channel.thumbnailUrl {
                let url = URL(string: _url)
                self.imageDownloadTask = self.channelImage.kf.setImage(with: url, placeholder: nil, options: nil)
            }
            
            self.channelNoLbl.text = "CH \(_event.channel.channelStbNumber!)"
            
            self.handleEvents()
        }
    }
}

fileprivate extension ChannelEventCell {
    @objc func handleEvents() {
        let current_futureEvents = self.event!.events.filter({$0.endDate.isFuture})
        self.event?.events = current_futureEvents
        if let currentEvent = current_futureEvents.first, !currentEvent.date.isFuture {
            
            self.setContent(event: currentEvent)
            self.refreshTimer = Timer.init(fireAt: currentEvent.endDate.forward(seconds: 1), interval: 0, target: self, selector: #selector(handleEvents), userInfo: nil, repeats: false)
            RunLoop.main.add(self.refreshTimer!, forMode: RunLoopMode.commonModes)
            
        } else if let upcoming = current_futureEvents.first {
            
            self.programLbl.text = "Upcoming: \(upcoming.program!)"
            self.categoryLbl.text = ""
            self.timeLbl.text = "Will air: \(DateFormatter.shortTime.string(from: upcoming.date))"
            self.refreshTimer = Timer.init(fireAt: upcoming.date, interval: 0, target: self, selector: #selector(handleEvents), userInfo: nil, repeats: false)
            RunLoop.main.add(self.refreshTimer!, forMode: RunLoopMode.commonModes)
        }
    }
    
    func setContent(event: Event) {
        self.setSelected(true, animated: true)
        UIView.animate(withDuration: 0.5, animations: { 
            self.programLbl.text = event.program
            self.categoryLbl.text = event.category ?? "--"
            self.timeLbl.text = "\(DateFormatter.shortTime.string(from: event.date)) - \(event.duration!)m"
            
        }) { (completed) in
            self.setSelected(false, animated: true)
        }

    }
}
