//
//  ChannelCell.swift
//  iChannel
//
//  Created by Danial on 15/06/2017.
//  Copyright © 2017 danialm. All rights reserved.
//

import UIKit
import Kingfisher
import RxSwift

class ChannelCell: UITableViewCell, SetupableCell {

    static var identifier: String! = "ChannelCell"
    
    fileprivate var channel: Channel?
    fileprivate var bag = DisposeBag()
    fileprivate var imageDownloadTask: RetrieveImageTask?
    var didChangeFavorite: (() -> ())?
    
    @IBOutlet weak var channelImage: UIImageView!
    @IBOutlet weak var numberLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var favBtn: UIButton!
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var languageLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func setup<T>(content: T) {
        self.channelImage.image = nil
        self.imageDownloadTask?.cancel()
        
        if let _content = content as? ChannelInfo {
            
            self.channel = _content
        
            self.nameLbl.text = _content.channelTitle
            self.numberLbl.text = "CH \(_content.channelStbNumber!)"
            self.categoryLbl.text = _content.category ?? ""
            self.languageLbl.text = _content.language ?? ""
            if let _url = _content.thumbnailUrl {
                let url = URL(string: _url)
                self.imageDownloadTask = self.channelImage.kf.setImage(with: url, placeholder: nil, options: nil)
            }
            
            self.loadFavState()
            
            self.bag = DisposeBag()
            
            let _ = FavoriteManager.shared.favoriteInfo.observable.subscribe(onNext:{ _ in
                self.loadFavState()
            }).addDisposableTo(self.bag)
        }
    }
    
    private func loadFavState() {
        if let _channel = self.channel {
            self.favBtn.isSelected = FavoriteManager.shared.isFavorite(channel: _channel)
        }
    }
    
    @IBAction func favTapped() {
        if let _channel = self.channel {
            if AuthenticationService().isLoggedIn() {
                FavoriteManager.shared.addOrRemove(channel: _channel)
                self.loadFavState()
            }
            self.didChangeFavorite?()
            
        }
    }
}
